<?php
session_start();
if(empty($_SESSION['login'])) {
	header('location:login.php');
	exit();
}
?>
<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Séries</title>
	<link rel="stylesheet" href="css/series.css">
	<script type="text/javascript" src="js/jquery-3.3.1.js"> </script>
</head>
<body>
	<section>
		<div class="lien"><a href="scripts_bdd/logout.php">Se déconnecter</a>
		</div>
		<div class="séries">
				<div id="firstLine">
					<a href="serie.php?no=1"> <img src="images/serie1.png" alt="perdu.cm" title="serie1" /> </a>	<!--Série 1-->
					<a href="serie.php?no=2"> <img src="images/serie2.png" alt="perdu.cm" title="serie2" /> </a>	<!--Série 2-->
					<a href="serie.php?no=3"> <img src="images/serie3.png" alt="perdu.cm" title="serie3" /> </a>	<!--Série 3-->
				</div>
				<div id="scdLine">
					<a href="serie.php?no=4"> <img src="images/serie4.png" alt="perdu.cm" title="serie4" /> </a>	<!--Série 4-->
					<a href="serie.php?no=5"> <img src="images/serie5.png" alt="perdu.cm" title="serie5" /> </a>	<!--Série 5-->
				</div>
				<div id="thirdLine">
					<a href="serie.php?no=Correction"> <img src="images/corriger.png" alt="perdu.cm" title="corriger" /> </a>	<!--Série Correction-->
				</div>
			</div>
	</section>
	<div id="license"><a href="http://www.freepik.com">Designed by brgfx / Freepik</a></div>
</body>
</html>
