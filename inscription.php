<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Inscription</title>
	<link rel="stylesheet" href="css/inscription.css">
	<script type="text/javascript" src="js/jquery-3.3.1.js"> </script>
</head>
<body>
	<div class="lien"><a href="login.php">Retour à la connexion</a>
	</div>
	<nav>
	<h1>Inscription</h1>
	</nav>
	<section>
		<div class="gris">
			<!--formulaire d'inscription-->
			<form action="inscription.php" method="POST">
				<div id="Prenom">
					Prénom <input type="texte" name="prenom" placeholder="Entrez votre prénom" style="width: 270px;"/>
				</div>
				<div id="Nom">
					Nom <input type="texte" name="nom" placeholder="Entrez votre nom" style="width: 270px;"/>
				</div>
				<div id="Login">
					Login <input type="texte" name="login" placeholder="Entrez votre login" style="width: 270px;"/>
				</div>
				<div id="Mdp">
					Mot de passe  <input type="password" name="mdp" placeholder="Entrez votre mot de passe" style="width: 270px;"/>
				</div>
				<div id="Confirmation">
					Confirmation  <input type="password" name="confirm" placeholder="Confirmez votre mot de passe" style="width: 270px;"/>
				</div>
				<div id="setBouton">
					<input type="submit" value="Valider l'inscription" name="ok">
				</div>
			</form>
		</div>
	</section>
	<?php
		//si le bouton "ok" est cliqué
		if (isset($_POST['ok'])) {
			//vérifier si tous les champs sont remplis
			if(!empty($_POST['prenom']) and !empty($_POST['nom']) and !empty($_POST['login']) and !empty($_POST['mdp']) and !empty($_POST['confirm'])) { 
				//confirmation du mot de passe
				if ($_POST['mdp']==$_POST['confirm']) {
					//connexion à la BD
					include_once('scripts_bdd/connexion.php');
					//inscription de l'utilisateur dans la BD
					$requete = $bdd->prepare('INSERT INTO users(prenom, nom, login, mdp) VALUES(?, ?, ?, ?)');
					$requete->execute(array($_POST['prenom'], $_POST['nom'], $_POST['login'], $_POST['mdp']));
					header('location:login.php');
				} else {	//si le mot de passe n'est pas confirmé
					echo "<script language='JavaScript'>alert('Les 2 mots de passe sont différents !')</script>";
				}
			} else {
				echo "<script language='JavaScript'>alert('Veillez saisir tous les champs !')</script>";
			}
		}
	?>
	<div id="license"><a href="http://www.freepik.com">Designed by brgfx / Freepik</a></div>
</body>
</html>
