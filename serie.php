<?php
session_start();
if(empty($_SESSION['login'])) {
	header('location:login.php');
	exit();
} else {
	//récupération du numéro de la série
	$numero = $_GET['no'];
	$mot= Array();
	//req SQL pour le set de mots
	include_once('scripts_bdd/connexion.php');
	if ($numero != "Correction") {
		$serie = "SELECT * FROM mots WHERE Id_serie ='$numero'";
		$tabmots = Array();
		if ($reponse = $bdd->query($serie)) {
			$cpt=1;
			while ($enr=$reponse->fetch()){
				$mot[$cpt]=$enr['mot'];	//récupération du tableau des mots de la série en cours
				$tabmots[$cpt]=$mot[$cpt];	//création du dictionnaire des mots de la série en cours
				$audio[$cpt]=$enr['audio'];	//récupération des liens audio de la série en cours
				$cpt++;
			} 
		} else{
			print "Échec de l'exécution de la requête";	
		} 
	} else {
		$logincour = $_SESSION['login'];
		$tabmots = Array();
		$motscours = "SELECT mot FROM mots_errones WHERE Login_user = '$logincour'";
		if ($reponse = $bdd->query($motscours)) {
			$cpt=1;
			while ($enr=$reponse->fetch()) {
				$mot[$cpt]=$enr['mot'];	//récupération du tableau des mots erronnés
				$tabmots[$cpt]=$mot[$cpt];	//création du dictionnaire des mots erronnés
				$serie = "SELECT DISTINCT api, audio FROM mots WHERE mot = '".$enr['mot']."'";
				if ($rep = $bdd->query($serie)){
					if ($recup=$rep->fetch()) {
						$audio[$cpt] = $recup['audio'];	//récupération des liens audio de la série en cours
					}
				} else {
					print "err2";
				}
				$cpt++;
			}
		} else {
			print "Échec de l'exécution de la requête de correction";
		}
	}
}
?>
<html>
<head>
<title>Exercice</title>
<meta charset="utf-8">
<script type="text/javascript" src="jquery-3.3.1.js"></script>
<script>
	var listemots = <?php echo json_encode($tabmots); ?>;	//création du json pour pouvoir manupuler le tableau dans le script
	var logincour = <?php echo '"'.$_SESSION['login'].'"';?>;	//récupération du login courant
	var motcour=1;	//l'indice du mot courant
	var phoncour=0;	//l'indice du phonème courant
	var seriecour = <?php if ($numero == "Correction") {echo "'".$numero."'";} else {echo $numero;}; ?>;	//la série en cours
	var err_phon=0;	//le nb d'erreurs phonétiques à envoyer dans la BD
	var err_graph=0;	//le nb d'erreurs graphémiques à envoyer dans la BD
	var mot = listemots[motcour];	//mot en cours
	$(document).ready(function(){
		//si le bouton "validé" est cliqué
		$(".valider").click(function() {
			id="#"+motcour;
			$(id).hide();
			motcour++;
			id="#"+motcour;
			$(id).show();
			phoncour=0;
			mot = listemots[motcour];
			//récupération de la transcription phonétique en cours
			  $.ajax ({
				url : 'scripts_bdd/getApi.php',
				method : 'POST',
				data : 'motcour='+motcour+'&serie='+seriecour+'&mot_err='+mot,
				dataType : 'html',
				success : function(reponse) {
					apicour = reponse;
				}
			  });
			//récupération du mot en cours
			  $.ajax ({
				url : 'scripts_bdd/getGraph.php',
				method : 'POST',
				data : 'motcour='+motcour+'&serie='+seriecour,
				dataType : 'html',
				success : function(reponse) {
					graphcour = reponse;
				}
			  });
			//vider les deux div après la validation du mot
			$("#output").empty();
			$("#resultat").empty();
			//si le mot produit est moins long que le mot attendu
			if (listemots[motcour-1].startsWith(contenudiv) == true && contenudiv.length < listemots[motcour-1].length) {
				playAudio('sons/reponse_fausse.wav');	//le poc
				err_graph = err_graph + 1;
			}
			// si le nb d'erreurs n'est pas à 0
			if ((err_phon != 0) || (err_graph != 0)) {
				//envoi des erreurs vers la BD
				  if (seriecour != "Correction") {
					  $.ajax ({
						  url : 'scripts_bdd/sendErr.php',
						  method : 'POST',
						  data : 'logincour='+logincour+'&graphcour='+graphcour+'&err_phon='+err_phon+'&err_graph='+err_graph,
						  dataType : 'html',
						  success : function(reponse) {
							  alert (reponse);
						  }
					  });
				}  
			} else {
				alert ("Bravo !\nMot suivant");
				//suppression du mot de la table des mots erronnés, si l'enfant ne fait plus d'erreur
				if (seriecour == "Correction") {
					  $.ajax ({
						  url : 'scripts_bdd/deleteErr.php',
						  method : 'POST',
						  data : 'logincour='+logincour+'&mot='+listemots[motcour-1],
						  dataType : 'html',
						  success : function(reponse) {
							  alert (reponse);
						  }
					  });
				}
			}
			//le nb d'erreurs est remis à 0 après chaque validation du mot
			err_phon=0;
			err_graph=0;
			
		});
		//vider la div des graphies choisies
		$(".recommencer").click(function() {
			$("#resultat").empty();
		});
	});
</script>
<script src="recupMots.js"></script>
<script src="audio.js"></script>
<link rel="stylesheet" href="css/serie.css"/>

</head>
<body class = <?php echo "'serie$numero'"?>>	<!--définition de la css de la page courante-->
<nav class="top">
	<div class="title"><?php echo "Série $numero"?></div>
	<div class="lien"><a href="series.php">Changer d'exercice</a></div>
	<div class="lien"><a href="scripts_bdd/logout.php">Se déconnecter</a>
</nav>
<div class="main">
	<div class="svg">
	<object data = "claviers/clavier.svg" type="image/svg+xml"></object>
	</div>
	<div class="column">
		<!--après chaque validation, l'audio est remplacé par le prochain dans la série-->
		<div class="play">
			<?php
				$i = 1;
				while ($i <= sizeof($mot)) {
					if ($i==1) {
						print "<img class = \"image\" id = \"$i\" src=\"images/play.png\" onclick=\"playAudio('$audio[$i]')\">";
					} else {
						print "<img class = \"image\" id = \"$i\" src=\"images/play.png\" onclick=\"playAudio('$audio[$i]')\" hidden >";
					}
					$i++;
				}
			?>

		</div>
		<!--la div où toutes les graphies possibles pour le phonème choisi aparaissent-->
		<div id="output" style="background-color: black; font-weight:bold; text-align:center; padding: 10px; margin: 10px; font-size: 2em; height: 160px; width: 800px;"></div>
		<!--la div où l'utilisateur écrit en couleurs; éditable-->
		<div id="resultat" contenteditable="true" style="background-color: black; font-weight:bold; text-align:center; padding: 10px; margin: 10px; font-size: 2em; height: 57px;"></div>

		<div id="boutons">
			<input class="recommencer" type="button" value=" Recommencer " onclick="recommencer()">
			<input class="valider" type="button" value="Valider">
		</div>
	</div>
</div>



</body>
</html>
