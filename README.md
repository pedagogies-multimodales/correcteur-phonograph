# Correcteur phonographémique

Application web contenant des exercices d'écriture en couleurs

Dans sa version 1.1 (février 2020), l'application représente un site web utilisable par les enseignants pratiquant la pédagogie multimodale. L'application contient les pages de connexion, d'inscription, de choix du set de mots, de l'exercice choisi (série 1-5 ou "Corriger mes erreurs"). Les utilisateurs peuvent s'inscrire et se connecter grâce à la base de données phpMyAdmin. Les mots des 5 premières séries de mots sont prédéfinis et communs à tous les utilisateurs. Par ailleurs, la page de "Corriger mes erreurs" est propre à chacun et dépend des erreurs de l'utilisateur concerné.

## Auteurs

**Capucine ANTOINE** : Étudiante en Master 2 Sciences du langage, parcours Industries de la langue

**Nadezhda RUMIANTCEVA** : Étudiante en Master 2 Sciences du langage, parcours Industries de la langue

Nous avons également repris des fichiers qui ont été créés par d'autres personnes :

`claviers/clavier.svg` et `data_phon2graph/data_phoneme-graphies_fr.scsv` de **Sylvain COULANGE** : Ingénieur d'études LIDILEM au sein du projet ANR IDEFI Innovalangues

`scripts_bdd/recupDico.php` et `recupMots.js` de **Lucie CHASSEUR** et **Rachel GAUBIL** : Étudiante en Master 2 Sciences du langage, parcours Industries de la langue

Les trois premiers scripts ont été intégrés tels qu'ils sont et n'ont pas été modifiés, tandis que le dernier code a été beaucoup enrichi par nous-mêmes.

## Installation

Pour pouvoir tester et éditer l'application, comme pour tout site web, il est impératif de mettre tout le package sur un serveur. Une fois tous les fichiers mis sur le serveur, un simple lien URL suffira pour rendre le site utilisable par tous les utilisateurs qui ont accès au serveur en question.

## Fonctionnalités principales

### Gestion des utilisateurs

L'inscription de nouveaux utilisateurs se fait grâce au script `inscription.php` qui remplit la table `users` de la base de données.

Une fois l'utilisateur enregistré, il peut se connecter. Le script responsable de cette action est `login.php`. Après la connexion, les actions de chaque apprenant sont suivies, elles définissent le contenu de la page "Corriger mes erreurs".

Le script `logout.php` permet de se déconnecter.

### Série 1-5

Cette page contient un clavier phonétique virtuel (`claviers/clavier.svg`). Il s'agit du clavier représentant les sons du français, mais un autre clavier peut être également intégré dans le script `serie.php`. Quant aux couleurs du clavier, elles sont définies dans `css/serie.css`. S'il y a une nécessité de changer les couleurs, il faudra les modifier dans la pseudo-racine `:root{}`.

Lorsque l'utilisateur clique sur un phonème, la liste de graphies correspondant à ce son s'affiche dans la zone noire à droite en haut. La couleur des graphies est exactement la même que celle du phonème.

Après la validation du mot à l'aide du bouton "Valider", la graphie du mot ainsi que le nombre d'erreurs sont envoyé vers la table `mots_errones` de la base de données. L'ajout se fait avec le script `scripts_bdd/sendErr.php`.

### Corriger mes erreurs

À la différence des pages des séries 1-5, cette page ne contient que les mots où l'utilisateur connecté s'est trompé. Pour enlever les mots erronés de la base de données (`scripts_bdd/deleteErr.php`), il faut qu'il n'ait plus d'erreurs.

### Data phon2graph

Le fichier `data_phon2graph/data_phoneme-graphie_fr.scsv` représente les correspondances phonème - liste de ses graphies en français. Ce fichier est intégré dans les séries à l'aide du script `scripts_bdd/recupGraphies.php` qui est intégré dans le code `recupMots.js`. Ces scripts permettent d'afficher la liste des graphèmes correspondante, une fois le phonème choisi par l'utilisateur.

### La base de données

Notre base de données contient trois tables : `users`, `mots` et `mots_errones`. La seule table dont le contenu peut être modifié manuellement est `mots` qui prédéfinit les jeux de mots pour chaque série. 
