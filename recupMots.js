//tableau associatif, SAMPA => API
var tabPhon = { 
    "rect_e_maj~":"ɛ̃", 
    "rect_a_maj~":"ɑ̃", 
    "rect_o_maj~":"ɔ̃",
    "rect_i":"i",
    "rect_y":"y",
    "rect_e":"e",
    "rect_e_maj":"ɛ",
    "rect_2":"ø",
    "rect_9":"œ",
    "rect_@":"ə",
    "rect_o_maj":"ɔ",
    "rect_a":"a",
    "rect_u":"u",
    "rect_o":"o",
    "rect_w":"w",
    "rect_h_maj":"ɥ",
    "rect_j":"j",
    "rect_k":"k",
    "rect_g":"ɡ",
    "rect_n_maj":"ɲ",
    "rect_r_maj":"ʁ",
    "rect_t":"t",
    "rect_d":"d",
    "rect_n":"n",
    "rect_l":"l",
    "rect_p":"p",
    "rect_b":"b",
    "rect_m":"m",
    "rect_f":"f",
    "rect_v":"v",
    "rect_s":"s",
    "rect_z":"z",
    "rect_s_maj":"ʃ",
    "rect_z_maj":"ʒ",
    "rect_we_maj~":"wɛ̃",
    "rect_wa":"wa",
    "rect_h_maj_i":"ɥi",
    "rect_ks":"ks",
    "rect_gz":"ɡz",
    "rect_nj":"ɲ",
    "rect_dz_maj":"dz",
  };
  
//tableau associatif : Id => class
var id2class = {
	"rect_e_maj~":"phon_e_maj_nas",
	"rect_a_maj~":"phon_a_maj_nas",
	"rect_o_maj~":"phon_o_maj_nas",
	"rect_i":"phon_i",
	"rect_y":"phon_y",
	"rect_e":"phon_e",
	"rect_e_maj":"phon_e_maj",
	"rect_2":"phon_2",
	"rect_9":"phon_9",
	"rect_@":"phon_arobase",
	"rect_o_maj":"phon_o_maj",
	"rect_a":"phon_a",
	"rect_u":"phon_u",
	"rect_o":"phon_o",
	"rect_w":"phon_w",
	"rect_h_maj":"phon_h_maj",
	"rect_j":"phon_j",
	"rect_k":"phon_k",
	"rect_g":"phon_g",
	"rect_n_maj":"phon_n_maj",
	"rect_r_maj":"phon_r_maj",
	"rect_t":"phon_t",
	"rect_d":"phon_d",
	"rect_n":"phon_n",
	"rect_l":"phon_l",
	"rect_p":"phon_p",
	"rect_b":"phon_b",
	"rect_m":"phon_m",
	"rect_f":"phon_f",
	"rect_v":"phon_v",
	"rect_s":"phon_s",
	"rect_z":"phon_z",
	"rect_s_maj":"phon_s_maj",
	"rect_z_maj":"phon_z_maj",
	"rect_we_maj~":"phon_we_maj_nas",
    "rect_wa":"phon_wa",
    "rect_h_maj_i":"phon_h_maj_i",
    "rect_ks":"phon_ks",
    "rect_nj":"phon_nj",
    "rect_dz_maj":"phon_dz_maj",
    "rect_gz":"phon_gz",
  };

//dictionnaire associatif phonétique => mot (ou liste de mots)
var dico = new Object();
//variable qui contiendra le mot en phonétique à rechercher dans le dictionnaire
var motARechercher="";
//variable qui contiendra le dictionnaire de listes de graphies
var listGraphies = new Object();
//variable qui contiendra le phonème en cours récupéré de la BD
var apicour = "";
//variable qui contiendra la graphie en cours récupéré de la BD
var graphcour = "";

//s'exécute au chargement de la page : récupération du dictionnaire
window.onload = function() {
  
  //requête à refaire
  $.ajax({
    url: 'scripts_bdd/recupDico.php',
    method: 'GET',
    dataType:'json',
    success:function(data){
      // met le json dans notre dictionnaire
      dico = data;
    }
  });
  
  //requête qui récupère la liste de graphies pour chaque phonème
  $.ajax ({
    url:'scripts_bdd/recupGraphies.php',
    method: 'POST',
    dataType: 'json',
    success : function(data) {
       listGraphies = data;
    }
  });
  
  $.ajax ({
	  url : 'scripts_bdd/getApi.php',
	  method : 'POST',
	  data : 'motcour='+motcour+'&serie='+seriecour+'&mot_err='+mot,
	  dataType : 'html',
	  success : function(reponse) {
		  apicour = reponse;
	  }
  });
  
  $.ajax ({
	  url : 'scripts_bdd/getGraph.php',
	  method : 'POST',
	  data : 'motcour='+motcour+'&serie='+seriecour,
	  dataType : 'html',
	  success : function(reponse) {
		  graphcour = reponse;
	  }
  });

}

//fonction permettant d'afficher la liste de graphèmes
function recupPhon(identifiant){
  document.getElementById("output").innerHTML = ''; // réinitialisation de la div d'output

  //vérification phonétique
  if (tabPhon[identifiant] != apicour[phoncour]) {
	  playAudio('sons/reponse_fausse.wav');	//le poc
	  err_phon = err_phon +1;
  } else {
	  phoncour = phoncour + 1;	//si pas d'erreur, alors passer au phonème suivant
  }
  

  //remplissage de la div "output" avec la liste des graphies
  for (let i in listGraphies[tabPhon[identifiant]]){
    graph = listGraphies[tabPhon[identifiant]][i]
    graphDiv = '<span onClick="writeGraph(\''+graph+'\',\''+identifiant+'\');" class="'+id2class[identifiant]+'">'+graph+'</span>  '
    document.getElementById("output").innerHTML += graphDiv;
    
  }
}


//fonction permettant d'écrire le choix de graphème
function writeGraph(graph,identifiant){
  graphDiv = '<span class="'+id2class[identifiant]+'">'+graph+'</span>'
  document.getElementById("resultat").innerHTML += graphDiv;
  contenudiv=document.getElementById("resultat").textContent;
  //vérification graphémique
  if (mot.startsWith(contenudiv) == false) {
	  playAudio('sons/reponse_fausse.wav');	//le poc
	  err_graph = err_graph + 1;
  }
}

//~ //effacer un phonème en cas d'erreur
function effacer(identifiant){
  //composition du mot en phonétique
  motARechercher=motARechercher.substring(0,motARechercher.length-1);
  document.getElementById('resultat').innerHTML=motARechercher;
}


function rechercher(){
  //si la suite de phonèmes existe
  if(dico[motARechercher]!=null){
    document.getElementById('zoneRetour').innerHTML+=" : "+dico[motARechercher];
  }
  else {
    document.getElementById('zoneRetour').innerHTML="Le mot \""+motARechercher+"\" n'existe pas.";
  }
  //réinitialisation du mot à rechercher
  motARechercher="";
}

//effectuer une nouvelle requête
function recommencer(){
  //réinitialisation du mot à rechercher
  motARechercher="";
  document.getElementById('zoneRetour').innerHTML="";
}
