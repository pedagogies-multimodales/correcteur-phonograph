<?php session_start (); //pour que le reste du code utilise la session en cours
	try {
		//PDO(lien vers la BDD)
		$bdd=new PDO('mysql:host=localhost;dbname=gaubilr','gaubilr','gaubilr');

		$dico=Array();

		$requete = 'SELECT * FROM DicoRaccourcis';
		$reponse = $bdd->query($requete);
		while ($donnees = $reponse -> fetch()){
			//enlever les "\r" à la fin des transcriptions normées
			$donnees['transcription_normee'] = trim($donnees['transcription_normee'],"\r");
			$dico[$donnees['transcription_normee']]=$donnees['forme'];
		}

		$json = json_encode($dico);
		
		//enlever la ligne ci-dessus provoque une erreur
		echo $json;
	}
	catch (Exception $e){
		die ("Erreur :".$e->getMessage());
		print "erreur";
	}
?>
