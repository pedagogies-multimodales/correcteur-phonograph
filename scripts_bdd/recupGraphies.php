<?php
	$graphies = Array();
	$dicograph = "";
	//ouverture du fichier scsv
	$fichier = "../data_phon2graph/data_phoneme-graphies_fr.scsv";
	if (is_file($fichier)) {
		$hfic = fopen($fichier, "r");
		while ($ligne=fgets($hfic)) {
			$ligne = trim ($ligne, "\n");
			$dicograph = explode (":", $ligne);	//phonème à gache du ":" et liste de graphies à droite
			$gr = explode (",",$dicograph[1]);	//split de la liste des graphies
			$graphies[$dicograph[0]]=$gr;
		}
		$json = json_encode($graphies);	//création du fichier json
		print $json;	//écriture du fichier json
	fclose($hfic);
	} else print "erreur";
?>

