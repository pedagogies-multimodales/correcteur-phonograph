<?php session_start(); ?>
<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Connexion</title>
	<link rel="stylesheet" href="css/login.css">
	<script type="text/javascript" src="js/jquery-3.3.1.js"> </script>
</head>
<body>
	<nav>
	<h1>Connexion</h1>
	</nav>
	<section>
		<div class="gris">
			<!--formulaire de la connexion-->
			<form action="login.php" method="POST">
				<div id="login">
					Login <input type="texte" name="login" placeholder="login"/>
				</div>
				<div id="mdp">
					Mot de passe <input type="password" name="mdp" placeholder="mot de passe"/>
				</div>
				<div id="gris2">
				<div id="connexion">
					<input type="submit" value="Se connecter" name="authentifier"/>
				</div>
				<div id="inscription">
					<input type="submit" value="S'inscrire" name="inscription"/>
				</div>
				</div>
			</form>
		</div>
	</section>
	<!--Se diriger vers la page d'inscription-->
	<?php
		if (isset($_POST ['inscription'])) {
			header ("location:inscription.php");
		}
	?>
	<!--Exécution de la requête de connexion-->
	<?php
		if (isset ($_POST['authentifier'])){
				include_once('scripts_bdd/connexion.php');
				$login=$_POST['login'];
				$mdp=$_POST['mdp'];
				$requete="SELECT login FROM users WHERE login='$login' AND mdp='$mdp'";
				if ($reponse = $bdd->query($requete)) {
					if ($reponse -> fetch()){
						$_SESSION['login']=strtolower($login);
						header("location:series.php");
					} else {
						print "Login ou mot de passe érronné";
					}
				} else{
					print "Échec de l'exécution de la requête";	
				}
			}
	?>
	<div id="license"><a href="http://www.freepik.com">Designed by brgfx / Freepik</a></div>
</body>
</html>
